module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
//     block: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       references: {
//         model: sequelize.models['block'],
//         key: 'number'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });
  
  User.belongsTo(
    sequelize.models['block'],
    {
      as: 'Block',
      foreignKey: {
        name: 'block',
        allowNull: false
      },
      targetKey: 'number',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
      
    }
  );

  return User
}
