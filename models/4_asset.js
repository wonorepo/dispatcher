module.exports = (sequelize, DataTypes) => {
  const Asset = sequelize.define('asset', {
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
//     ,
//     owner: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       references: {
//         model: sequelize.models['user'],
//         key: 'id'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     },
//     block: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       references: {
//         model: sequelize.models['block'],
//         key: 'number'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });

  Asset.belongsTo(
    sequelize.models['user'],
    {
      as: 'Owner',
      foreignKey: {
        name: 'owner',
        allowNull: false
      },
      targetKey: 'id',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );
  
  Asset.belongsTo(
    sequelize.models['block'],
    {
      as: 'Block',
      foreignKey: {
        name: 'block',
        allowNull: false
      },
      targetKey: 'number',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );

  return Asset
}

