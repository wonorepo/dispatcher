module.exports = (sequelize, DataTypes) => {
  const DealData = sequelize.define('dealdata', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: 'id_deal_uni'
    },
//     deal: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       unique: 'id_deal_uni',
//       references: {
//         model: sequelize.models['deal'],
//         key: 'id'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     },
    signedByA: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    signedByB: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    confirmedByA: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    confirmedByB: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    cancelledByA: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    cancelledByB: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    infoHash: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false
    },
    start: {
      type: DataTypes.DATE,
      allowNull: false
    },
    end: {
      type: DataTypes.DATE,
      allowNull: false
    },
    redeemed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }
//     ,
//     block: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       references: {
//         model: sequelize.models['block'],
//         key: 'number'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });
  
  DealData.belongsTo(
    sequelize.models['deal'],
    {
      as: 'Deal',
      foreignKey: {
        name: 'deal',
        allowNull: false
      },
      targetKey: 'id',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );

  DealData.belongsTo(
    sequelize.models['block'],
    {
      as: 'Block',
      foreignKey: {
        name: 'block',
        allowNull: false
      },
      targetKey: 'number',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );
  
  return DealData
}

