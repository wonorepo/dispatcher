module.exports = (sequelize, DataTypes) => {
  const UserData = sequelize.define('userdata', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: 'id_user_uni'
    },
//     user: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       unique: 'id_user_uni',
//       references: {
//          model: sequelize.models['user'],
//          key: 'id'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     },
    infoHash: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false
    }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });
  
  UserData.belongsTo(
    sequelize.models['user'],
    {
      as: 'User',
      foreignKey: {
        name: 'user',
        allowNull: false
      },
      targetKey: 'id',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );
  
  UserData.belongsTo(
    sequelize.models['block'],
    {
      as: 'Block',
      foreignKey: {
        name: 'block',
        allowNull: false
      },
      targetKey: 'number',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );

  return UserData
}
