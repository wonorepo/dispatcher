module.exports = (sequelize, DataTypes) => {
  const Block = sequelize.define('block', {
    number: {
      primaryKey: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      unique: true
    },
    parentHash: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    hash: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    timestamp: {
      type: DataTypes.DATE,
      allowNull: false
    }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });

  return Block
}
