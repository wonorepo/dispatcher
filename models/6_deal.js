module.exports = (sequelize, DataTypes) => {
  const Deal = sequelize.define('deal', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: 'id_asset_uni'
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    }
//     ,
//     partyA: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       unique: false,
//       references: {
//         model: sequelize.models['asset'],
//         key: 'id'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     },
//     partyB: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       unique: false,
//       references: {
//         model: sequelize.models['user'],
//         key: 'id'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     },
//     block: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       references: {
//         model: sequelize.models['block'],
//         key: 'number'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });

  Deal.belongsTo(
    sequelize.models['asset'],
    {
      as: 'PartyA',
      foreignKey: {
        name: 'partyA',
        allowNull: false
      },
      targetKey: 'id',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );  

  Deal.belongsTo(
    sequelize.models['user'],
    {
      as: 'PartyB',
      foreignKey: {
        name: 'partyB',
        allowNull: false
      },
      targetKey: 'id',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );  
  
  Deal.belongsTo(
    sequelize.models['block'],
    {
      as: 'Block',
      foreignKey: {
        name: 'block',
        allowNull: false
      },
      targetKey: 'number',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );
  
  return Deal
}

