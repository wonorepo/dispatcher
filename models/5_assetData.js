module.exports = (sequelize, DataTypes) => {
  const AssetData = sequelize.define('assetdata', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      unique: 'id_asset_uni'
    },
//     asset: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       unique: 'id_asset_uni',
//       references: {
//         model: sequelize.models['asset'],
//         key: 'id'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     },
    price: {
      type: DataTypes.NUMERIC(36, 18),
      allowNull: false
    },
    unit: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    infoHash: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: false
    }
//     ,
//     block: {
//       type: DataTypes.INTEGER,
//       allowNull: false,
//       references: {
//         model: sequelize.models['block'],
//         key: 'number'
//       },
//       onUpdate: 'RESTRICT',
//       onDelete: 'CASCADE'
//     }
  },
  {
    timestamps: false,
    underscored: true,
    freezeTableName: true
  });

  AssetData.belongsTo(
    sequelize.models['asset'],
    {
      as: 'Asset',
      foreignKey: {
        name: 'asset',
        allowNull: false
      },
      targetKey: 'id',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );
  
  AssetData.belongsTo(
    sequelize.models['block'],
    {
      as: 'Block',
      foreignKey: {
        name: 'block',
        allowNull: false
      },
      targetKey: 'number',
      onUpdate: 'RESTRICT',
      onDelete: 'CASCADE'
    }
  );

  
  return AssetData
}

