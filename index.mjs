import Config from './services/config.js';

let config = Config.getInstance();
console.log(config);

import Web3 from 'web3';
import db from './models';
import delay from 'delay';


import EventListener from './services/EventListener.js';

// Event handlers
// Should be added for any specific event you gonna handle
import userAddedHandler from './handlers/userAdded.mjs';
import userUpdatedHandler from './handlers/userUpdated.mjs';
import assetCreatedHandler from './handlers/assetCreated.mjs';
import assetUpdatedHandler from './handlers/assetUpdated.mjs';
import assetDeletedHandler from './handlers/assetDeleted.mjs';
import dealCreatedHandler from './handlers/dealCreated.mjs';
import dealSignedByAHandler from './handlers/dealSignedByA.mjs';
import dealSignedByBHandler from './handlers/dealSignedByB.mjs';
import dealConfirmedByAHandler from './handlers/dealConfirmedByA.mjs';
import dealConfirmedByBHandler from './handlers/dealConfirmedByB.mjs';
import dealDisputedByAHandler from './handlers/dealDisputedByA.mjs';
import dealDisputedByBHandler from './handlers/dealDisputedByB.mjs';
import dealRedeemedHandler from './handlers/dealRedeemed.mjs';
import dealCancelledByAHandler from './handlers/dealCancelledByA.mjs';
import dealCancelledByBHandler from './handlers/dealCancelledByB.mjs';

// Last block we assume to be indexed
let startFrom = config.startBlock;

let web3;

// Returns last block from relational DB. See ./models/1_block.js for more info
async function getLastBlock() {
  const lastBlock = await db.sequelize.models.block.findOne({
    order: [['number', 'DESC']],
    limit: 1
  });
  if (lastBlock)
    return lastBlock;
  else
    return undefined;
}

// Returns last block from blockchain. See web3 documentation for more info.
async function getBlock(number) {
  return new Promise((resolve, reject) => {
    web3.eth.getBlock(number, (err, block) => {
      if (err)
        reject(err)
      else
        resolve(block);
    });
  });
}

db.sequelize.sync() // Re-creating relational DB schema if needed.
.then(() => {
  // Choosing web3 provider depending on URL scheme
  if (config.blockchain.search('^wss://') > -1)
    web3 = new Web3(new Web3.providers.WebsocketProvider(config.blockchain));
  else
    web3 = new Web3(new Web3.providers.HttpProvider(config.blockchain));
})
.then(async () => {
  let lastBlock = await getLastBlock(); // lastBlock from relational DB
  if (lastBlock) {
    console.log(`\x1b[32mLast indexed block \x1b[1m${lastBlock.number}\x1b[0m`);
  }
  else {
    lastBlock = await getBlock(startFrom); // Using startBlock in case of empty relational DB
  }

  
  // Creating event listener object
  const listener = new EventListener(web3);
  // ...and registering all events it should listen to.
  // Specify event name in form 'ContractName.EventName'
  // so you can distinguish events with same names from individual contracts
  await listener.registerEvent('UserStorage.userAdded',   userAddedHandler);
  await listener.registerEvent('UserStorage.userUpdated', userUpdatedHandler);
  await listener.registerEvent('AssetFactory.assetCreated', assetCreatedHandler);
  await listener.registerEvent('Asset.assetUpdated', assetUpdatedHandler);
  await listener.registerEvent('Asset.assetDeleted', assetDeletedHandler);
  await listener.registerEvent('DealFactory.dealCreated', dealCreatedHandler);
  await listener.registerEvent('Deal.dealSignedByB', dealSignedByBHandler);
  await listener.registerEvent('Deal.dealConfirmedByA', dealConfirmedByAHandler);
  await listener.registerEvent('Deal.dealConfirmedByB', dealConfirmedByBHandler);
  await listener.registerEvent('Deal.dealDisputedByA', dealDisputedByAHandler);
  await listener.registerEvent('Deal.dealDisputedByB', dealDisputedByBHandler);
  await listener.registerEvent('Deal.dealRedeemed', dealRedeemedHandler);
  await listener.registerEvent('Deal.dealCancelledByA', dealCancelledByAHandler);
  await listener.registerEvent('Deal.dealCancelledByB', dealCancelledByBHandler);

  let rawBlock;
  while(1) {
    try {
      while(rawBlock = await getBlock(lastBlock.number + 1)) {
        if (rawBlock.parentHash == lastBlock.hash) {
          rawBlock.timestamp = new Date(rawBlock.timestamp * 1000).toISOString(); // converting JS timestamp to ISO in-place just to have no further headache
          // creating block record in DB
          // all attributes we need to store have same names in blockchain and our DB
          const newBlock = await db.sequelize.models.block.create(rawBlock);  
          console.log(`\x1b[36mBlock ${newBlock.number}\x1b[0m`);
          
          // Handling events if there are any
          await listener.handleEvents(newBlock.number);
          
          // Updating last handled block
          lastBlock = newBlock;
        }
        // If new block has wrong parent hash we remove topmost block from our DB.
        // This way we search for first block with correct hash - the fork point.
        // Even if we delete all blocks from our DB (i.e. in case of switching to different chain).
        else {
          console.log(`\x1b[33mParent hash mismatch. Deleting block \x1b[1m${lastBlock.number}\x1b[0m`);
          await lastBlock.destroy();
          lastBlock = await getLastBlock();
        }
      }
    }
    catch (e) {
      if (e.message === 'Invalid JSON RPC response: ""')
        // Web3 provider often breaks connection. So it is not an error at all.
        console.log(`\x1b[33mWeb3 provider had closed connection\x1b[0m`);
      else
        throw(e);
    }
    //console.log(`\x1b[32mWaiting for new block...\x1b[0m`);
    await delay(5 * 1000);
    //console.log(`\x1b[1A\x1b[2K\x1b[1A`);
  }
})
// General error reporting with stack trace.
// It's safe to just throw an exception and you'll get all the stack trace in logs.
.catch((e) => {
  console.error(`\x1b[35;1m${e.message}\x1b[22m\n${e.stack}\x1b[0m`);
  process.exit(1);
});
