const config = {
  provider: 'wss://kovan.infura.io/ws',
  port: 5432
}

export default config
