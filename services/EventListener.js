const Config = require("../services/config");
const axios = require("axios");

const config = Config.getInstance();

class EventListener {
 
  constructor (web3) {
    this._web3 = web3;
    this._abi = [];       // contract ABIs
    this._contracts = []; // contract representation
    this.events = [];
  };
  
  // Loads contract ABI from config server by contract name
  async getABI(contractName) {
    if (!this._abi[contractName]) {
      try {
        // URL is hard coded. It isn't assumed to be changed in any circumstances.
        const abiResponse = await axios.get(`https://env.wono.io/contracts/abi/${contractName}.json`);
        this._abi[contractName] = abiResponse.data;
        console.log(`\x1b[36mABI for \x1b[1m${contractName}\x1b[22m loaded\x1b[0m`);
      }
      catch (e) {
        console.error(`\x1b[31mFailed loading ABI for \x1b[1m${contractName}\x1b[0m`);
        throw(e); // This is fatal error. Bailing out.
      }
    }
    return this._abi[contractName];
  }

  // Loads contract addressed from config server.
  // Note that different configs may (and probably should) use different
  // set of contracts sharing same ABI and deployed per config.
  async loadAddresses() {
    if (this._contracts.length == 0) {  // Checking if we have already loaded contract addresses
      try {
        const contractsResponse = await axios.get(config.contractsURL);
        this._contracts = contractsResponse.data;
        console.log(`\x1b[36mContract addresses loaded\x1b[0m`);
      }
      catch (e) {
        console.error(`\x1b[31mFailed loading contract addresses\x1b[0m`);
        throw(e); // This is fatal error. Bailing out.
      }
    }
  }
  
  // Returns contract address by name.
  // Loads contract addresses if needed.
  async getAddress(contractName) {
    await this.loadAddresses();
    if (config.contracts && config.contracts[contractName] != undefined)
      this._contracts[contractName] = config.contracts[contractName];
    if (this._contracts[contractName])
      return this._contracts[contractName].addr;
    else
      return undefined;
  }

/*  

Registers event and sets handler.

Arguments:

  event
    string containg contract name and event name in dot-notation (i.e. "MyContract.ExpectedEvent")
    
  handler
    async function (web3, event). See ./handlers/assetCreated.mjs for more info.
    
Returns event signature which is useful in debugging.

*/
  async registerEvent(event, handler) {
    const [contractName, eventName] = event.split('.');
    const abi = await this.getABI(contractName);          // contract ABI
    const address = await this.getAddress(contractName);  // contract address
    const abiEvent = abi.filter(entry => entry.type == 'event' && entry.name == eventName)[0];  // event declaration from ABI
    const signature = this._web3.eth.abi.encodeEventSignature(abiEvent);  // decoding event signature
    // Creating event object and storing it in listener
    this.events[signature] = {
      contract: contractName,
      address: address, // contract address
      abi: abiEvent,
      signature: signature,
      handler: handler
    };
    // Extracting event signature arguments and converting to human-readable form
    const args = abiEvent.inputs.map((arg) => {return `${arg.type}${arg.indexed ? ' indexed' : ''} ${arg.name}`}).join(', ');
    console.log(`\x1b[32mEvent \x1b[1m${this.events[signature].contract}.${this.events[signature].abi.name}(${args})\x1b[22m registered as \x1b[1m${signature}\x1b[0m`);
    return signature;
  }

  // Returns list of signatures of events we're listening to.
  getTopics() {
    return [Object.keys(this.events)];
  }
  
  // Returns unique list of contract addresses events from which we're listening to.
  getAddresses() {  // FIXME this should be done once due to save CPU
    const seen = {};
    return Object.values(this.events)
      .filter((event) => {return event.address != undefined})
      .map((event) => {return event.address})
      .filter((arg) => {return (seen[arg] ? false : seen[arg] = arg)})
  }
  
  // Gets all filtered list of events in specified block.
  // Only registered events are returned.
  async getEvents(blockNumber) {
    return this._web3.eth.getPastLogs({fromBlock: blockNumber, toBlock: blockNumber, topics: this.getTopics()});
  }
  
  // Wrapper for getTransaction. Seems to be part of legacy code.
  async getTx(txHash) {
    return this._web3.eth.getTransaction(txHash);
  }
  
  // Runs handlers for every registered event in specified block.
  async handleEvents(blockNumber) {
    const events = await this.getEvents(blockNumber);  // events from specified block
    const handlers = []; // handlers to run
    // Adding handlers to queue
    for (let i in events) {
      const event = events[i];
      const abi = this.events[event.topics[0]].abi; // event ABI declaration
      events[i].transaction = await this.getTx(events[i].transactionHash); // transaction for particular event
      // Prevent handling of events from unknown (not listed in config) contracts.
      // These may be either events from contracts of other config or cooked by intruder.
      // In both cases just put a line in log and ignore them.
      if (this.events[event.topics[0]].address == undefined || this.events[event.topics[0]].address.toLowerCase() == event.address.toLowerCase())
      {
        console.log(`\x1b[32mEvent \x1b[1m${this.events[event.topics[0]].contract}.${abi.name}\x1b[22m occured\x1b[0m`);
        // Add handler to queue
        handlers.push(this.events[events[i].topics[0]].handler(this._web3, events[i]));
      }
      else
        console.log(`\x1b[33mEvent \x1b[1m${this.events[event.topics[0]].contract}.${abi.name}\x1b[22m ignored due to wrong source:\n`
          + `    got \x1b[1m${event.address}\x1b[22m expected \x1b[1m${this.events[event.topics[0]].address}\x1b[0m`);
    }
    // Running handlers
    return Promise.all(handlers);
  }
};

module.exports = EventListener;
