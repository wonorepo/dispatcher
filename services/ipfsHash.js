const bs58 = require('bs58');

function fromBytes32(bytes32) {
  return bs58.encode(new Buffer.from('1220' + bytes32.substr(2), 'hex'));
}

function toBytes32(ipfs) {
  return '0x' + bs58.decode(ipfs).toString('hex').substr(4);
}

module.exports = {fromBytes32, toBytes32};
