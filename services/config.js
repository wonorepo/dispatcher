const syncLoader = require("sync-request");

// Use NODE_ENV environment variable to select which particular config you want to use.

// This is a sigleton class
// Configuration is being loaded once and used anywhere with calling Config.getInstance()

const Config = (function () {
  var instance;
    
  function loadRemote(conf) {
    // URL is hard coded. It isn't assumed to be changed in any circumstances.
    const request = syncLoader('GET', `https://env.wono.io/services/${conf}.json`);
    return JSON.parse(request.getBody());
  }

  function loadLocal(conf) {
    const rv = require(`../config/${conf}.json`);
    return rv;
  }

    
  function createInstance() {
    let conf = process.env['NODE_ENV'] || 'default';
    const remote = loadRemote(conf);
    const local = loadLocal(conf);
    return Object.assign({}, remote, local);
  }

  return {
    getInstance: function () {
      if (!instance) {
        instance = createInstance();
      }
      return instance;
    }
  };
})();

module.exports = Config;
