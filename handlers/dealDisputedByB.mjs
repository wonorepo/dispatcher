import models from '../models';
import axios from 'axios';
import Config from '../services/config.js';
const config = Config.getInstance();
import ipfsHash from '../services/ipfsHash.js';
import ipfsApi from 'ipfs-api';

/*****************************************************************************

IMPORTANT:

See boilerplate assetCreated.mjs

*****************************************************************************/

export default async function dealConfirmedByBHandler (web3, event) {
  // NOTE: this event recieves only indexed params

  // Checking if transaction is sent by registered user.
  const user = await models.user.findOne({
    where: {
      address: event.transaction.from.toLowerCase()
    }
  });
  if (!user) {
    console.log(`\x1b[33mUser \x1b[1m${event.transaction.from.toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }
  
  // Checking if deal exists. Ownership is checked inside smart contract.
  const deal = await models.deal.findOne({
    where: {
      address: String(`0x${event.topics[1].substr(-40)}`).toLowerCase()
    }
  });
  if (!deal) {
    console.log(`\x1b[33mDeal \x1b[1m${String(event.topics[1].substr(-40)).toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }

  // Retrieving latest volatile data record
  let dealData = await models.dealdata.findOne({
    where: {
      deal: deal.id
    },
    limit: 1,
    order: [['id', 'DESC']]
  });
  dealData = dealData.get();
  dealData.id = undefined;
  dealData.confirmedByB = -1;
  dealData.block = event.blockNumber;
  const newDealData = await models.dealdata.create(dealData);
  
  let dealInfo = {
      confirmedByB: dealData.confirmedByB
  }
  
  axios({
    method : 'put',
    url    : `${config.api}/api/deals/${deal.address}`,
    headers: {
      'X-Signature'  : `api-key ${config.apiKey}`,
      'X-Eth-Address': user.address,
      'Content-Type' : 'application/json'
    },
    data   : dealInfo
  })
  .then((r) => { 
    console.log(`\x1b[32mDeal ${deal.address} sent to API\x1b[0m`);
  })
  .catch((e) => {
    if (e.response && e.response.status === 409)
      console.log(`\x1b[33mDeal \x1b[1m${deal.address}\x1b[22m already registered in API\x1b[0m`);
    else {
      console.log(`\x1b[31mSending deal ${deal.address} to API failed: ${e}\x1b[0m`);
      throw(e);
    }
  })
}

