import models from '../models';
import axios from 'axios';
import Config from '../services/config.js';
const config = Config.getInstance();
import ipfsHash from '../services/ipfsHash.js';
import ipfsApi from 'ipfs-api';

/*****************************************************************************

IMPORTANT:

See boilerplate assetCreated.mjs

*****************************************************************************/

export default async function userUpdatedHandler (web3, event) {
  // Decoding params
  // IMPORTANT: See parameter decoding section assetCreated.mjs
  const params = web3.eth.abi.decodeParameters(['bytes32'], event.data);
  // Checking if transaction is sent by registered user.
  const user = await models.user.findOne({
    where: {
      address: String(`0x${event.topics[1].substr(-40)}`).toLowerCase()
    }
  });
  if (!user) {
    console.log(`\x1b[33mUser \x1b[1m${event.transaction.from.toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }
  
  // NOTE: all attributes in volatile data record are changed. So no need to retrieve latest volatile data record.
  const newUserData = await models.userdata.create({
    user: user.id,
    infoHash: ipfsHash.fromBytes32(params[0]),
    block: event.blockNumber
  });
  const ipfs = await ipfsApi(config.ipfs);
  let userInfo;
  try {
    userInfo = JSON.parse(await ipfs.files.cat(newUserData.infoHash));
    if (typeof userInfo != 'object')
      throw("infoHash is corrupted");
  }
  catch (e) {
    console.log(`\x1b[31mUser profile \x1b[1m${newUser.address}\x1b[22m data \x1b[1m${newUserData.infoHash}\x1b[22m is corrupted\x1b[0m`);
    await newUserData.destroy();
    return;
  }

  // Enriching user data before sending it to API.
  // This kind of copy-paste-change part of code that is present in every handler.
  userInfo.eth_address = user.address;
  userInfo.ipfs_hash   = user.infoHash;
  userInfo.is_deleted  = false
  
  // Sending enriched data to API
  axios({
    method : 'put',
    url    : `${config.api}/api/profiles/${user.address}`,
    headers: {
      'X-Signature'  : `api-key ${config.apiKey}`,
      'X-Eth-Address': user.address,
      'Content-Type' : 'application/json'
    },
    data   : userInfo
  })
  .then((r) => { 
    console.log(`\x1b[32mUser profile ${user.address} updates sent to API\x1b[0m`);
  })
  .catch((e) => {
    if (e.response && e.response.status === 409)
      console.log(`\x1b[33mUser profile \x1b[1m${user.address}\x1b[22m already registered in API\x1b[0m`);
    else {
      console.log(`\x1b[31mSending user ${user.address} to API failed: ${e}\x1b[0m`);
      throw(e);
    }
  })
}
