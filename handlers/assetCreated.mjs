import models from '../models';
import axios from 'axios';
import Config from '../services/config.js';
const config = Config.getInstance();
import ipfsHash from '../services/ipfsHash.js';
import ipfsApi from 'ipfs-api';

/*****************************************************************************

In general event handler should perform the following steps:

1. Create static data record in DB (in case of create event)
   or retrieve latest volatile data record (in case of update or delete event)
2. Create volatile data record in DB. Every unchanged attribute should be
   copied from lates volatile data record if any.
3. Retrieve data from IPFS
4. Enrich data with fields from blockchain if any required
5. Send enriched data to API

In case of error on any step handler should destroy all data records in DB
that were created during handling this particular event. All dependent data
records will be deleted by foreign key constraint (i.e. volatile data record
after static data record)

******************************************************************************

Arguments:

  web3
    blockchain provider connection instance
    
  event
    particular event object to handle

*****************************************************************************/

export default async function assetCreatedHandler (web3, event) {
  // Decoding params.
  /*

  IMPORTANT:

  Argument types should be in sync with event arguments list.
  Arguments marked as 'indexed' are stored in topics in order they appear in event signature.
  topics[0] is event signature itself. See web3 manual for more info.
  
  */
  const params = web3.eth.abi.decodeParameters(['bytes32', 'uint256', 'int8', 'bytes4'], event.data);
  
  // Checking if transaction is sent by registered user.
  const user = await models.user.findOne({
    where: {
      address: event.transaction.from.toLowerCase()
    }
  });
  // Ignore transactions from unregistered users.
  if (!user) {
    console.log(`\x1b[33mUser \x1b[1m${event.transaction.from.toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }
  
  // Creating asset static data record
  const newAsset = await models.asset.create({
    address: String(`0x${event.topics[1].substr(-40)}`).toLowerCase(),
    owner: user.id,
    block: event.blockNumber
  });
  // Creating asset volatile data record and linking it to static data record
  const newAssetData = await models.assetdata.create({
    asset: newAsset.id,
    price: web3.utils.fromWei(params[1]),
    unit: params[2],
    infoHash: ipfsHash.fromBytes32(params[0]),
    block: event.blockNumber
  });
  
  // Retrieve asset data from IPFS
  // This kind of copy-paste-change part of code that is present in every handler.
  const ipfs = await ipfsApi(config.ipfs);
  let assetInfo;
  try {
    assetInfo = JSON.parse(await ipfs.files.cat(newAssetData.infoHash));
    if (typeof assetInfo != 'object')
      throw("infoHash is corrupted");
  }
  catch (e) {
    console.log(`\x1b[31mAsset \x1b[1m${newAsset.address}\x1b[22m data \x1b[1m${newAssetData.infoHash}\x1b[22m is corrupted\x1b[0m`);
    // Removing asset static data record if we couldn't load and parse volatile data.
    // Actually volatile data record will be deleted by foreign key constraint.
    await newAsset.destroy();
    return;
  }

  // Enriching asset volatile data befor sending it to API.
  // This kind of copy-paste-change part of code that is present in every handler.
  assetInfo.eth_address = newAsset.address;
  assetInfo.owner       = newAsset.owner;
  assetInfo.ipfs_hash   = newAssetData.infoHash;
  assetInfo.is_deleted  = false;
  
  // Sending enriched data to API
  axios({
    method : 'post',
    url    : `${config.api}/api/assets/`,
    headers: {
      'X-Signature'  : `api-key ${config.apiKey}`,
      'X-Eth-Address': user.address,
      'Content-Type' : 'application/json'
    },
    data   : assetInfo
  })
  .then((r) => { 
    console.log(`\x1b[32mAsset ${newAsset.address} sent to API\x1b[0m`);
  })
  .catch((e) => {
    if (e.response && e.response.status === 409)
      console.log(`\x1b[33mAsset \x1b[1m${newAsset.address}\x1b[22m already registered in API\x1b[0m`);
    else {
      console.log(`\x1b[31mSending asset ${newAsset.address} to API failed: ${e}\x1b[0m`);
      throw(e);
    }
  })
}
