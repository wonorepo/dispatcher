import models from '../models';
import axios from 'axios';
import Config from '../services/config.js';
const config = Config.getInstance();
import ipfsHash from '../services/ipfsHash.js';
import ipfsApi from 'ipfs-api';

/*****************************************************************************

IMPORTANT:

See boilerplate assetCreated.mjs

*****************************************************************************/

export default async function assetUpdatedHandler (web3, event) {
  // Decoding params
  // IMPORTANT: See parameter decoding section assetCreated.mjs
  const params = web3.eth.abi.decodeParameters(['bytes32'], event.data);
  
  // Checking if transaction is sent by registered user.
  const user = await models.user.findOne({
    where: {
      address: event.transaction.from.toLowerCase()
    }
  });
  if (!user) {
    console.log(`\x1b[33mUser \x1b[1m${event.transaction.from.toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }

  // Checking if asset exists and is owned by the user who has sent the transaction
  const asset = await models.asset.findOne({
    where: {
      address: String(`0x${event.topics[1].substr(-40)}`).toLowerCase(),
      owner: user.id
    },
    include: [
      {
        model: models.user,
        as: 'Owner'
      }
    ]
  });
  if (!asset) {
    console.log(`\x1b[33mAsset \x1b[1m${String(event.topics[1].substr(-40)).toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }
  
  // Retrieving latest volatile data
  let assetData = await models.assetdata.findOne({
    where: {
      asset: asset.id
    },
    limit: 1,
    order: [['id', 'DESC']]
  });
  
  // Sterilizing data object to get rid of Sequelize stuff
  assetData = assetData.get();
  // Change attributes that is specific to this particular record.
  // This kind of copy-paste-change part of code that is present in every update and delete handler.
  assetData.id = undefined;
  assetData.infoHash = ipfsHash.fromBytes32(params[0]);
  assetData.block = event.blockNumber;
  const newAssetData = await models.assetdata.create(assetData);
  
  // Retrieve asset data from IPFS
  // This kind of copy-paste-change part of code that is present in every handler.
  const ipfs = await ipfsApi(config.ipfs);
  let assetInfo;
  try {
    assetInfo = JSON.parse(await ipfs.files.cat(newAssetData.infoHash));
    if (typeof assetInfo != 'object')
      throw("infoHash is corrupted");
  }
  catch (e) {
    console.log(`\x1b[31mAsset \x1b[1m${asset.address}\x1b[22m data \x1b[1m${newAssetData.infoHash}\x1b[22m is corrupted\x1b[0m`);
    await newAssetData.destroy();
    return;
  }
  
  // Enrich asset data
  // This kind of copy-paste-change part of code that is present in every handler.
  assetInfo.eth_address = asset.address;
  assetInfo.owner       = asset.Owner.address;
  assetInfo.ipfs_hash   = newAssetData.infoHash;
  assetInfo.is_deleted  = false;

  // Sending enriched data to API
  await axios({
    method : 'put',
    url    : `${config.api}/api/assets/${asset.address}`,
    headers: {
      'X-Signature'  : `api-key ${config.apiKey}`,
      'X-Eth-Address': user.address,
      'Content-Type' : 'application/json'
    },
    data   : assetInfo
  })
  .then((r) => { 
    console.log(`\x1b[32mAsset ${asset.address} updates sent to API\x1b[0m`);
  })
  .catch((e) => {
    if (e.response && e.response.status === 409)
      console.log(`\x1b[33mAsset \x1b[1m${asset.address}\x1b[22m already registered in API\x1b[0m`);
    else if (e.response && e.response.status === 410)
      console.log(`\x1b[33mAsset \x1b[1m${asset.address}\x1b[22m removed from API\x1b[0m`);
    else {
      console.log(`\x1b[31mSending asset ${asset.address} to API failed: ${e}\x1b[0m`);
      throw(e);
    }
  })
}
