import models from '../models';
import axios from 'axios';
import Config from '../services/config.js';
const config = Config.getInstance();
import ipfsHash from '../services/ipfsHash.js';
import ipfsApi from 'ipfs-api';

/*****************************************************************************

IMPORTANT:

See boilerplate assetCreated.mjs

*****************************************************************************/

export default async function dealCreatedHandler (web3, event) {
  // Decoding params
  // IMPORTANT: See parameter decoding section assetCreated.mjs
  const params = web3.eth.abi.decodeParameters(['uint', 'bytes32', 'uint32', 'uint32', 'bytes4'], event.data);
  // Checking if transaction is sent by registered user.
  const user = await models.user.findOne({
    where: {
      address: event.transaction.from.toLowerCase()
    }
  });
  if (!user) {
    console.log(`\x1b[33mUser \x1b[1m${event.transaction.from.toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }

  // Checking if pary A exists and owned by user who have sent the transaction.
  // NOTE: party A is an asset
  const partyA = await models.asset.findOne({
    where: {
      address: String(`0x${event.topics[2].substr(-40)}`).toLowerCase(),
      owner: user.id
    }
  });
  if (!partyA) {
    console.log(`\x1b[33mAsset \x1b[1m${String(event.topics[2].substr(-40)).toLowerCase()}\x1b[22m is not registered or is not owned by \x1b[1m${event.transaction.from.toLowerCase()}\x1b[0m`);
    return;
  }
  // Checking if party B exists.
  // NOTE: party B is user.
  const partyB = await models.user.findOne({
    where: {
      address: String(`0x${event.topics[3].substr(-40)}`).toLowerCase(),
    }
  });
  if (!partyB) {
    console.log(`\x1b[33mUser \x1b[1m${event.topics[3].substr(-40).toLowerCase()}\x1b[22m is not registered\x1b[0m`);
    return;
  }
  
  const newDeal = await models.deal.create({
    address: String(`0x${event.topics[1].substr(-40)}`).toLowerCase(),
    partyA: partyA.id,
    partyB: partyB.id,
    block: event.blockNumber
  });
  const newDealData = await models.dealdata.create({
    deal: newDeal.id,
    infoHash: ipfsHash.fromBytes32(params[1]),
    start: new Date(params[2] * 1000).toISOString(),
    end: new Date(params[3] * 1000).toISOString(),
    block: event.blockNumber
  });
  
  const ipfs = await ipfsApi(config.ipfs);
  let dealInfo = {};
  try {
    dealInfo = JSON.parse(await ipfs.files.cat(newDealData.infoHash));
    dealInfo.ipfs_hash = newDealData.infoHash;
    if (typeof dealInfo != 'object')
      throw("infoHash is corrupted");
  }
  catch (e) {
    console.log(`\x1b[31mDeal \x1b[1m${newDeal.address}\x1b[22m data \x1b[1m${newDealData.infoHash}\x1b[22m is corrupted\x1b[0m`);
    await newDeal.destroy();
    return;
  }

  dealInfo.partyA       = user.address;
  dealInfo.partyB       = partyB.address;
  dealInfo.eth_address  = newDeal.address;
  dealInfo.signedByA    = true;
  dealInfo.signedByB    = false;
  dealInfo.confirmedByA = 0;
  dealInfo.confirmedByB = 0;
  dealInfo.owner        = user,
  dealInfo.is_deleted   = false;
  
  axios({
    method : 'post',
    url    : `${config.api}/api/deals/`,
    headers: {
      'X-Signature'  : `api-key ${config.apiKey}`,
      'X-Eth-Address': user.address,
      'Content-Type' : 'application/json'
    },
    data   : dealInfo
  })
  .then((r) => { 
    console.log(`\x1b[32mDeal ${newDeal.address} sent to API\x1b[0m`);
  })
  .catch((e) => {
    if (e.response && e.response.status === 409)
      console.log(`\x1b[33mDeal \x1b[1m${newDeal.address}\x1b[22m already registered in API\x1b[0m`);
    else {
      console.log(`\x1b[31mSending deal \x1b[1m${newDeal.address}\x1b[22m to API failed: ${e}\x1b[0m`);
      throw(e);
    }
  })
}
