ARG NODE_VERSION=10.11
FROM node:${NODE_VERSION}

COPY package.json yarn.lock /code/
RUN cd /code && yarn install --production

COPY . /code
WORKDIR /code

ARG ENV=dev
ENV NODE_ENV=${ENV}

CMD ["yarn", "start"]
